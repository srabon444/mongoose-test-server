const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const productSchema = require('../schemas/productSchema');

//Product Model Create
const Product = new mongoose.model("Product", productSchema);

// GET ALL THE PRODUCTS
router.get('/product', async (req, res) => {
    try {
        // const price = req.query.price; // For dynamic price
        // await Product.find({ price: { $lt: price } });
        // For logical operator:
        let products;
        const price = req.query.price;
        const title = req.query.title;

        if (price && title) { // Logical Operator
            products = await Product.find({
                $and: [
                    { price: { $gte: price } },
                    { title: { $eq: title } }
                ]
            });
        } else {
            // To Sort Ascending:  }).sort({price: 1});
            // To Sort Ascending:  }).sort({price: -1});
            products = await Product.find().sort({ price: 1 });
        }
        // To limit: await Product.find().limit(2);
        // To find products using operator: ref: mongo doc
        // await Product.find({price: {$lte:1200}});
        // Most importantly I will get these operator dynamically from clients body
        if (products) {
            res.status(200).send({
                success: true,
                message: "Return All Product",
                data: products
            });
        } else {
            res.status(400).send({
                success: false,
                message: "products Not Found"
            });
        }
    } catch (error) {
        if (error) {
            console.log("Error: ", error);
            res.status(500).json({
                error: "There was a server side error"
            });
        } else {
            res.status(200).json({
                message: "Products were get successfully"
            });
        }
    }
});

// GET A PRODUCTS by ID
router.get('/product/:id', async (req, res) => {
    try {
        const id = req.params.id;

        const product = await Product.findOne({ _id: id });
        // To limit response all data:
        // await Product.findOne({_id: id}).select({title:1, price:1});
        // To remove _id as response
        // await Product.findOne({_id: id}).select({title:1, _id: 0});

        if (product) {
            res.status(200).send({
                success: true,
                message: "Return Single Product",
                data: product
            });
        } else {
            res.status(400).send({
                success: false,
                message: "product Not Found"
            });
        }
    } catch (error) {
        if (error) {
            console.log("Error: ", error);
            res.status(500).json({
                error: "There was a server side error"
            });
        } else {
            res.status(200).json({
                message: "Product was get successfully"
            });
        }
    }
});

//POST A PRODUCT
router.post('/product', async (req, res) => {
    try {
        // console.log("hit")
        const newProduct = new Product(req.body);
        const productData = await newProduct.save();
        res.status(201).send(productData);
    } catch (error) {
        if (error) {
            console.log("Error: ", error);
            res.status(500).json({
                error: "There was a server side error"
            });
        } else {
            res.status(200).json({
                message: "Product was inserted successfully"
            });
        }
    }
});

//POST MULTIPLE PRODUCTS
router.post('/all', async (req, res) => {

});

//PUT PRODUCT
router.put('/product/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const updatedProduct = await Product.updateOne(
            { _id: id },
            {
                $set: {
                    title:req.body.title,
                    price: req.body.price,
                    description: req.body.description
                }
            },
            {new : true}
            // This line returns the immediate updated data to the user
        );
        if (updatedProduct) {
            res.status(200).send({
                success: true,
                message: "Product Updated Successfully",
                data: updatedProduct
            });
        } else {
            res.status(400).send({
                success: false,
                message: "product Not Found"
            });
        }

    } catch (error) {
        if (error) {
            console.log("Error: ", error.message);
            res.status(500).json({
                error: "There was a server side error"
            });
        }
    }
});

//DELETE PRODUCT
router.delete('/product/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const product = await Product.deleteOne({ _id: id });
        if (product) {
            res.status(200).send({
                success: true,
                message: "Product Deleted",
                data: product
            });
        } else {
            res.status(400).send({
                success: false,
                message: "product Was Not Found with this ID"
            });
        }
    } catch (error) {
        if (error) {
            console.log("Error: ", error.message);
            res.status(500).json({
                error: "There was a server side error"
            });
        }
    }

});

module.exports = router;