const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const todoSchema = require('../schemas/todoSchema');

const Todo = new mongoose.model("Todo", todoSchema);

//GET ALL THE TODOS
router.get('/todos', async (req, res) => {
    try {
        const todos = await Todo.find();
        if (todos) {
            res.status(200).send({
                success: true,
                message: "All Todos Found",
                data: todos
            });
        } else {
            res.status(400).send({
                success: false,
                message: "Todos Not Found"
            });
        }
    } catch (error) {
        if (error) {
            console.log("Error: ", error);
            res.status(500).json({
                error: "There was a server side error"
            });
        } else {
            res.status(200).json({
                message: "Todos were get successfully"
            });
        }
    }
});

// GET A TODOx by ID
router.get('/todo/:id', async (req, res) => {
    try {
        const id = req.params.id;
        const todo = await Todo.find({ _id: id }).select({ _id: 0 });
        console.log({ todo })
        if (todo) {
            res.status(200).send({
                success: true,
                message: `${todo[0].title} Found`,
                data: todo
            });
        } else {
            res.status(400).send({
                success: false,
                message: "Todo Not Found"
            });
        }
    } catch (error) {
        if (error) {
            console.log("Error: ", error);
            res.status(500).json({
                error: "There was a server side error"
            });
        } else {
            res.status(200).json({
                message: "Todo was get successfully"
            });
        }
    }
});

//POST A TODOx
router.post('/todos', async (req, res) => {
    const newTodo = new Todo(req.body);
    await newTodo.save((err) => {
        if (err) {
            res.status(500).json({
                error: "There was a server side error"
            });
        } else {
            res.status(200).json({
                message: "Todo was inserted successfully"
            });
        }
    });
});

//POST MULTIPLE TODOS
router.post('/todos/all', async (req, res) => {
    await Todo.insertMany(req.body, (err) => {
        if (err) {
            res.status(500).json({
                error: "There was a server side error"
            });
        } else {
            res.status(200).json({
                message: "Todos were inserted successfully"
            });
        }
    });
});

//PUT TODOx
router.put('/todos/:id', async (req, res) => {
    try {
        const id = req.params.id;
        await Todo.updateOne({ _id: id },
            {
                $set: {
                    title: req.body.title,
                    status: req.body.status
                },
            });
        res.status(200).json({
            message: "Todo was updated successfully"
        });
    } catch (error) {
        res.status(500).json({
            error: "There was a server side error"
        });
    }
});

//DELETE TODOx
router.delete('/todos/:id', async (req, res) => {
    try {
        const id = req.params.id;
        await Todo.deleteOne({ _id: id });
        res.status(200).json({
            message: "Todo was deleted successfully"
        });
    } catch (error) {
        res.status(500).json({
            error: "There was a server side error"
        });
    }
});

module.exports = router;