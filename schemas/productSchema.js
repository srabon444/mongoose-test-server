const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
    title: {
        type: String,
        required: [true, "Product title required"],
        minLength: [2, "Minimum length of title should be at least 2"],
        trim: true
    },
    price: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    //Custom Validation
    /*mobile: {
        type: String,
        required: [true, "Mobile Number is Required"],
        validate: {
            validator: function (v) {
                return /\d{3}-\d{3}-\d{5}/.test(v);
            },
            message: props => `${props.value} is not a valid mobile number!`
        }
    }*/
});

module.exports = productSchema;