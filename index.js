const express = require('express');
const mongoose = require('mongoose');
const todoHandler = require('./routeHandler/todoHandler');
const productHandler = require('./routeHandler/producthandler');
const port = process.env.PORT || 5000;
require('dotenv').config();

//Express app initialization
const app = express();

//middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//database connection with mongoose
const connect = async () => {
    try {
        await mongoose.connect(`mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASS}@cluster0.atbzeoy.mongodb.net/mongooseDemo`, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        console.log("Database Connection Successful");
    } catch (error) {
        console.log("DB is not connected");
        console.log(error.message);
    }
};

//application routes
app.use('', productHandler);
app.use('', todoHandler);


// //default error handler
// function errorHandler(err, req, res, next) {
//     if (res.headersSent) {
//         return next(err);
//     }
//     res.status(500).json({ error: err });
// }

app.listen(port, async () => {
    console.log(`Server is Running at http://localhost:${port}`);
    await connect();
});

//This portion doesn't work, because productHandler has a same route
//To make it work, just comment out product Handler same get route
app.get('/', (req, res) => {
    res.send("Welcome to mongoose test server");
});

